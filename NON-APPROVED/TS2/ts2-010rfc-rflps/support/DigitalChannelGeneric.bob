<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Digital Channel Block</name>
  <width>1120</width>
  <height>800</height>
  <scripts>
    <script file="EmbeddedPy">
      <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

current_digi_mode = PVUtil.getInt(pvs[3])
sim_values = ['ZERO','ONE']

# Initial configuration of the DIGITAL PRE PROCESS
if (current_digi_mode < 2):
	pvs[1].write("Yes")
	pvs[2].write(sim_values[current_digi_mode])
else:
	pvs[1].write("No")
]]></text>
      <pv_name>loc://${PREFIX}:${DEVICE}:initialPV</pv_name>
      <pv_name trigger="false">loc://${PREFIX}:${DEVICE}:simDigiEna</pv_name>
      <pv_name trigger="false">loc://${PREFIX}:${DEVICE}:simDigiSig</pv_name>
      <pv_name trigger="false">${PREFIX}:${DEVICE}:PP-Mode-RB</pv_name>
    </script>
  </scripts>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1121</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <line_color>
      <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
      </color>
    </line_color>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
    <corner_width>4</corner_width>
    <corner_height>4</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>TITLE</class>
    <text>Digital Pre-Processing Block Configuration</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>700</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_9</name>
    <text>$(PREFIX):$(DEVICE)</text>
    <x>20</x>
    <y>100</y>
    <width>640</width>
    <height>25</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Label_8</name>
    <class>HEADER2</class>
    <pv_name>$(PREFIX):$(DEVICE).DESC</pv_name>
    <x>20</x>
    <y>65</y>
    <width>530</width>
    <height>35</height>
    <font>
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="xyplot" version="2.0.0">
    <name>X/Y Plot</name>
    <x>590</x>
    <y>460</y>
    <width>510</width>
    <height>290</height>
    <foreground_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </background_color>
    <grid_color>
      <color red="77" green="77" blue="77">
      </color>
    </grid_color>
    <show_legend>false</show_legend>
    <x_axis>
      <title>Time (us)</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>8000.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Digital Input Value</title>
        <autoscale>false</autoscale>
        <log_scale>false</log_scale>
        <minimum>-0.5</minimum>
        <maximum>1.5</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="BOLD" size="10.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name>$(traces[0].y_pv)</name>
        <x_pv></x_pv>
        <y_pv>${PREFIX}:${DEVICE}:Wave</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color name="BLUE" red="79" green="228" blue="250">
          </color>
        </color>
        <line_width>1</line_width>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Digital Pre-Process Block Configuration</name>
    <x>20</x>
    <y>460</y>
    <width>541</width>
    <height>180</height>
    <style>1</style>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </foreground_color>
    <background_color>
      <color name="Button_Background" red="236" green="236" blue="236">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="combo" version="2.0.0">
      <name>Combo Box</name>
      <pv_name>${PREFIX}:${DEVICE}:PP-Mode</pv_name>
      <x>180</x>
      <y>112</y>
      <width>150</width>
      <height>20</height>
      <background_color>
        <color name="Write_Background" red="224" green="217" blue="202">
        </color>
      </background_color>
      <rules>
        <rule name="Change PV name if simulation is enabled" prop_id="pv_name" out_exp="false">
          <exp bool_exp="pvStr0 == &quot;Yes&quot;">
            <value></value>
          </exp>
          <pv_name>loc://${PREFIX}:${DEVICE}:simDigiEna</pv_name>
        </rule>
        <rule name="Disable if simulationis enabled" prop_id="enabled" out_exp="false">
          <exp bool_exp="pvStr0 == &quot;Yes&quot; ">
            <value>false</value>
          </exp>
          <pv_name>loc://${PREFIX}:${DEVICE}:simDigiEna</pv_name>
        </rule>
      </rules>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <items>
        <item>== 0</item>
        <item>== 1</item>
      </items>
      <items_from_pv>false</items_from_pv>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_3</name>
      <pv_name>${PREFIX}:${DEVICE}:PP-Mode-RB</pv_name>
      <x>353</x>
      <y>112</y>
      <width>150</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_10</name>
      <class>CAPTION</class>
      <text>Pre-processing Mode</text>
      <x>10</x>
      <y>112</y>
      <width>160</width>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_68</name>
      <text>Enable Interlock Simulation</text>
      <x>39</x>
      <y>19</y>
      <width>210</width>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>Combo Box_2</name>
      <pv_name>loc://${PREFIX}:${DEVICE}:simDigiEna</pv_name>
      <x>259</x>
      <y>19</y>
      <width>80</width>
      <height>20</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <background_color>
        <color name="Write_Background" red="224" green="217" blue="202">
        </color>
      </background_color>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <items>
        <item>No</item>
        <item>Yes</item>
      </items>
      <items_from_pv>false</items_from_pv>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_41</name>
      <pv_name>loc://${PREFIX}:${DEVICE}:simDigiEna</pv_name>
      <x>429</x>
      <y>12</y>
      <width>60</width>
      <visible>false</visible>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <format>1</format>
      <vertical_alignment>1</vertical_alignment>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_69</name>
      <text>Simulated Interlock Signal</text>
      <x>39</x>
      <y>49</y>
      <width>210</width>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>Combo Box_3</name>
      <pv_name>loc://${PREFIX}:${DEVICE}:simDigiSig</pv_name>
      <x>259</x>
      <y>49</y>
      <width>80</width>
      <height>20</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <background_color>
        <color name="Write_Background" red="224" green="217" blue="202">
        </color>
      </background_color>
      <rules>
        <rule name="Set PV name if Simulation is ON" prop_id="pv_name" out_exp="false">
          <exp bool_exp="pvStr0 == &quot;Yes&quot;">
            <value>${PREFIX}:${DEVICE}:PP-Mode</value>
          </exp>
          <pv_name>loc://${PREFIX}:${DEVICE}:simDigiEna</pv_name>
        </rule>
        <rule name="Disable if Simulation is OFF" prop_id="enabled" out_exp="false">
          <exp bool_exp="pvStr0 == &quot;Yes&quot; ">
            <value>true</value>
          </exp>
          <exp bool_exp="pvStr0 == &quot;No&quot;">
            <value>false</value>
          </exp>
          <pv_name>loc://${PREFIX}:${DEVICE}:simDigiEna</pv_name>
        </rule>
      </rules>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <items>
        <item>ZERO</item>
        <item>ONE</item>
      </items>
      <items_from_pv>false</items_from_pv>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_42</name>
      <pv_name>loc://${PREFIX}:${DEVICE}:simDigiSig</pv_name>
      <x>429</x>
      <y>42</y>
      <width>60</width>
      <visible>false</visible>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <format>1</format>
      <vertical_alignment>1</vertical_alignment>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_16</name>
      <x>30</x>
      <y>82</y>
      <width>470</width>
      <height>1</height>
      <line_width>1</line_width>
      <line_color>
        <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
        </color>
      </line_color>
      <points>
        <point x="0.0" y="0.0">
        </point>
        <point x="470.00000000000006" y="0.0">
        </point>
      </points>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_74</name>
      <text>Readback value</text>
      <x>353</x>
      <y>90</y>
      <width>150</width>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <vertical_alignment>2</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>EPICS IOC Details</name>
    <x>10</x>
    <y>650</y>
    <width>560</width>
    <height>110</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </foreground_color>
    <widget type="bool_button" version="2.0.0">
      <name>Boolean Button</name>
      <pv_name>${PREFIX}:${DEVICE}:ChEnable</pv_name>
      <y>39</y>
      <height>20</height>
      <off_label>Disabled</off_label>
      <on_label>Enabled</on_label>
      <show_led>false</show_led>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <tooltip>Enable/Disable PV updates</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_1</name>
      <pv_name>${PREFIX}:${DEVICE}:ChEnable-RB</pv_name>
      <x>99</x>
      <y>39</y>
      <width>30</width>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_19</name>
      <pv_name>$(PREFIX):$(IOC_):STAT-RB</pv_name>
      <x>169</x>
      <y>39</y>
      <width>140</width>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <precision>0</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <rules>
        <rule name="Change background" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==4">
            <value>
              <color red="255" green="255" blue="153">
              </color>
            </value>
          </exp>
          <exp bool_exp="pv0==7">
            <value>
              <color red="179" green="230" blue="179">
              </color>
            </value>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
        <rule name="Change border" prop_id="border_color" out_exp="false">
          <exp bool_exp="pv0==4">
            <value>
              <color name="Text" red="25" green="25" blue="25">
              </color>
            </value>
          </exp>
          <exp bool_exp="pv0==7">
            <value>
              <color name="Text" red="25" green="25" blue="25">
              </color>
            </value>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_40</name>
      <pv_name>${PREFIX}:${IOC_}:HistBufferStatus-RB</pv_name>
      <x>339</x>
      <y>39</y>
      <width>140</width>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <precision>0</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <rules>
        <rule name="Alarm ENDED" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==3">
            <value>
              <color name="RED-BACKGROUND" red="225" green="192" blue="188">
              </color>
            </value>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_65</name>
      <text>PV updates</text>
      <x>19</x>
      <y>9</y>
      <width>90</width>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_66</name>
      <text>IOC Runtime status</text>
      <x>169</x>
      <y>9</y>
      <width>140</width>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_67</name>
      <text>FW ring buffer status</text>
      <x>339</x>
      <y>9</y>
      <width>140</width>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_70</name>
    <text>Current FIM State</text>
    <x>756</x>
    <y>10</y>
    <width>140</width>
    <height>30</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_43</name>
    <pv_name>$(PREFIX):$(IOC_):INTS-RB</pv_name>
    <x>896</x>
    <y>10</y>
    <width>170</width>
    <height>30</height>
    <font>
      <font family="Liberation Sans" style="BOLD" size="16.0">
      </font>
    </font>
    <precision>0</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_27</name>
    <pv_name>loc://${PREFIX}:${DEVICE}:initialPV(0)</pv_name>
    <x>20</x>
    <y>130</y>
    <width>90</width>
    <visible>false</visible>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_10</name>
    <actions>
      <action type="open_display">
        <file>DigitalWavePlot.bob</file>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Open new window</text>
    <x>980</x>
    <y>437</y>
    <width>120</width>
    <height>20</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Input Channel Logic Configuration</name>
    <x>20</x>
    <y>160</y>
    <width>1080</width>
    <height>250</height>
    <style>1</style>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </foreground_color>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_19</name>
      <x>685</x>
      <y>62</y>
      <width>260</width>
      <height>2</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GRAY" red="169" green="169" blue="169">
        </color>
      </line_color>
      <points>
        <point x="0.0" y="0.0">
        </point>
        <point x="260.0" y="0.0">
        </point>
      </points>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_11</name>
      <class>CAPTION</class>
      <text>Current Value:</text>
      <x>10</x>
      <y>42</y>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_2</name>
      <pv_name>$(PREFIX):$(DEVICE):InputVal</pv_name>
      <x>22</x>
      <y>68</y>
      <width>70</width>
      <height>30</height>
      <off_label>Low</off_label>
      <on_label>High</on_label>
      <foreground_color>
        <color name="WHITE" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <square>true</square>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_4</name>
      <text>HV ON Interlock Logic</text>
      <x>575</x>
      <y>32</y>
      <width>110</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <background_color>
        <color name="ORANGE-BACKGROUND" red="226" green="207" blue="189">
        </color>
      </background_color>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_5</name>
      <text>RF ON Interlock Logic</text>
      <x>751</x>
      <y>32</y>
      <width>110</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <background_color>
        <color name="YELLOW-BACKGROUND" red="226" green="216" blue="193">
        </color>
      </background_color>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_17</name>
      <x>806</x>
      <y>97</y>
      <width>1</width>
      <height>38</height>
      <line_width>2</line_width>
      <line_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </line_color>
      <arrows>2</arrows>
      <arrow_length>10</arrow_length>
      <points>
        <point x="0.0" y="38.0">
        </point>
        <point x="0.0" y="0.0">
        </point>
      </points>
      <rules>
        <rule name="Invisible if channel is bypassed" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 == 0">
            <value>false</value>
          </exp>
          <pv_name>$(PREFIX):$(DEVICE):HVToRF-RB</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_18</name>
      <x>630</x>
      <y>97</y>
      <width>1</width>
      <height>38</height>
      <line_width>2</line_width>
      <line_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </line_color>
      <arrows>2</arrows>
      <arrow_length>10</arrow_length>
      <points>
        <point x="0.0" y="38.0">
        </point>
        <point x="0.0" y="0.0">
        </point>
      </points>
      <rules>
        <rule name="Invisible if signal is bypassed" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 == 0">
            <value>false</value>
          </exp>
          <pv_name>$(PREFIX):$(DEVICE):IdleToHV-RB</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_22</name>
      <x>630</x>
      <y>135</y>
      <width>1</width>
      <height>10</height>
      <line_width>2</line_width>
      <line_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </line_color>
      <points>
        <point x="0.0" y="0.0">
        </point>
        <point x="0.0" y="10.0">
        </point>
        <point x="0.0" y="10.0">
        </point>
      </points>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_23</name>
      <x>806</x>
      <y>135</y>
      <width>1</width>
      <height>10</height>
      <line_width>2</line_width>
      <line_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </line_color>
      <points>
        <point x="0.0" y="0.0">
        </point>
        <point x="0.0" y="10.0">
        </point>
        <point x="0.0" y="10.0">
        </point>
      </points>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_5</name>
      <x>92</x>
      <y>140</y>
      <width>70</width>
      <height>1</height>
      <line_width>2</line_width>
      <line_color>
        <color name="PRIMARY" red="0" green="148" blue="202">
        </color>
      </line_color>
      <arrows>2</arrows>
      <arrow_length>10</arrow_length>
      <points>
        <point x="0.0" y="0.0">
        </point>
        <point x="70.0" y="0.0">
        </point>
      </points>
      <rules>
        <rule name="Disable if PP is simulated" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 &lt; 2">
            <value>false</value>
          </exp>
          <pv_name>${PREFIX}:${DEVICE}:PP-Mode-RB</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Polyline_15</name>
      <x>415</x>
      <y>142</y>
      <width>375</width>
      <height>1</height>
      <line_width>2</line_width>
      <line_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </line_color>
      <arrow_length>10</arrow_length>
      <points>
        <point x="0.0" y="0.0">
        </point>
        <point x="393.7499999999999" y="0.0">
        </point>
      </points>
    </widget>
    <widget type="ellipse" version="2.0.0">
      <name>Ellipse</name>
      <x>801</x>
      <y>137</y>
      <width>10</width>
      <height>10</height>
      <line_width>1</line_width>
      <line_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </line_color>
      <background_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </background_color>
      <rules>
        <rule name="Change background if disable" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0 == 0">
            <value>
              <color name="BACKGROUND" red="220" green="225" blue="221">
              </color>
            </value>
          </exp>
          <pv_name>$(PREFIX):$(DEVICE):HVToRF-RB</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="ellipse" version="2.0.0">
      <name>Ellipse_1</name>
      <x>625</x>
      <y>137</y>
      <width>10</width>
      <height>10</height>
      <line_width>1</line_width>
      <line_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </line_color>
      <background_color>
        <color name="PRIMARY-DARK" red="31" green="83" blue="102">
        </color>
      </background_color>
      <rules>
        <rule name="Change background" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0 == 0">
            <value>
              <color name="BACKGROUND" red="220" green="225" blue="221">
              </color>
            </value>
          </exp>
          <pv_name>$(PREFIX):$(DEVICE):IdleToHV-RB</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Group_4</name>
      <x>165</x>
      <y>77</y>
      <width>250</width>
      <height>90</height>
      <style>3</style>
      <widget type="action_button" version="3.0.0">
        <name>Rectangle_6</name>
        <text></text>
        <y>40</y>
        <width>250</width>
        <height>50</height>
        <tooltip></tooltip>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_24</name>
        <text>Digital Pre-Processing Block</text>
        <x>5</x>
        <y>15</y>
        <width>188</width>
        <height>21</height>
        <auto_size>true</auto_size>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_72</name>
        <text>PP FUNCTION</text>
        <y>55</y>
        <width>250</width>
        <height>21</height>
        <font>
          <font family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </font>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <rules>
          <rule name="Change label" prop_id="text" out_exp="false">
            <exp bool_exp="pv0 == 0">
              <value>ZERO (Simulated interlock)</value>
            </exp>
            <exp bool_exp="pv0 == 1">
              <value>ONE (Bypassed interlock)</value>
            </exp>
            <exp bool_exp="pv0 == 2 ">
              <value>IF INPUT = HIGH RESULT = OK</value>
            </exp>
            <exp bool_exp="pv0 == 3">
              <value>IF INPUT = LOW, RESULT = OK</value>
            </exp>
            <pv_name>${PREFIX}:${DEVICE}:PP-Mode-RB</pv_name>
          </rule>
        </rules>
      </widget>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_8</name>
      <text>INPUT SIGNAL</text>
      <x>22</x>
      <y>105</y>
      <width>70</width>
      <height>70</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Group</name>
      <x>735</x>
      <y>157</y>
      <width>140</width>
      <height>30</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="bool_button" version="2.0.0">
        <name>Boolean Button_3</name>
        <pv_name>$(PREFIX):$(DEVICE):HVToRF</pv_name>
        <width>110</width>
        <off_label>Bypassed</off_label>
        <on_label>Enabled</on_label>
        <show_led>false</show_led>
        <font>
          <font family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_4</name>
        <pv_name>$(PREFIX):$(DEVICE):HVToRF-RB</pv_name>
        <x>110</x>
        <width>30</width>
        <height>30</height>
        <off_color>
          <color name="MINOR" red="252" green="242" blue="17">
          </color>
        </off_color>
        <square>true</square>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Group</name>
      <x>559</x>
      <y>157</y>
      <width>140</width>
      <height>30</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="led" version="2.0.0">
        <name>LED_3</name>
        <pv_name>$(PREFIX):$(DEVICE):IdleToHV-RB</pv_name>
        <x>110</x>
        <width>30</width>
        <height>30</height>
        <off_color>
          <color name="MINOR" red="252" green="242" blue="17">
          </color>
        </off_color>
        <square>true</square>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="bool_button" version="2.0.0">
        <name>Boolean Button_2</name>
        <pv_name>$(PREFIX):$(DEVICE):IdleToHV</pv_name>
        <width>110</width>
        <off_label>Bypassed</off_label>
        <on_label>Enabled</on_label>
        <show_led>false</show_led>
        <font>
          <font family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_6</name>
      <pv_name>$(PREFIX):$(DEVICE):FastIntStat</pv_name>
      <x>455</x>
      <y>127</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </off_color>
      <on_color>
        <color name="OK" red="61" green="216" blue="61">
        </color>
      </on_color>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_71</name>
      <text>Current Result</text>
      <x>415</x>
      <y>93</y>
      <width>110</width>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Picture</name>
      <file>images/interlock.svg</file>
      <x>949</x>
      <y>37</y>
      <width>50</width>
      <height>50</height>
      <rules>
        <rule name="change_icon" prop_id="file" out_exp="false">
          <exp bool_exp="pv0 &gt; 0">
            <value>support/interlock tripped.svg</value>
          </exp>
          <pv_name>$(PREFIX):$(DEVICE):Ilck</pv_name>
        </rule>
      </rules>
      <tooltip>Click to reset interlock</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_11</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(PREFIX):$(DEVICE):resetIlck</pv_name>
      <text></text>
      <x>949</x>
      <y>37</y>
      <width>51</width>
      <height>50</height>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <transparent>true</transparent>
      <rules>
        <rule name="enable if interlock" prop_id="enabled" out_exp="false">
          <exp bool_exp="pv0 &gt; 0">
            <value>true</value>
          </exp>
          <pv_name>$(PREFIX):$(DEVICE):Ilck</pv_name>
        </rule>
      </rules>
      <enabled>false</enabled>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_13</name>
      <actions>
        <action type="open_display">
          <file>AnalogPMortemPlot.bob</file>
          <target>window</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>Open last Post-mortem</text>
      <x>919</x>
      <y>97</y>
      <width>110</width>
      <height>40</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_14</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(PREFIX):$(DEVICE):resetIlck</pv_name>
      <text>Reset</text>
      <x>1000</x>
      <y>52</y>
      <width>61</width>
      <height>26</height>
      <visible>false</visible>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <rules>
        <rule name="enable if interlock" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 &gt; 0">
            <value>true</value>
          </exp>
          <pv_name>$(PREFIX):$(DEVICE):Ilck</pv_name>
        </rule>
      </rules>
    </widget>
  </widget>
</display>
