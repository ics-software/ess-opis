<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title></title>
    <save_changes>true</save_changes>
    <show_legend>false</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>false</grid>
    <scroll>false</scroll>
    <update_period>3.0</update_period>
    <scroll_step>5</scroll_step>
    <start>-1 hours 0.0 seconds</start>
    <end>2019-08-23 09:19:00</end>
    <archive_rescale>NONE</archive_rescale>
    <foreground>
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
    </foreground>
    <background>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
    </background>
    <title_font>Cantarell|16|1</title_font>
    <label_font>Cantarell|11|1</label_font>
    <scale_font>Cantarell|10|0</scale_font>
    <legend_font>Cantarell|10|0</legend_font>
    <axes>
        <axis>
            <visible>true</visible>
            <name>Value 1</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>0.0</min>
            <max>25.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 2</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>50.0</min>
            <max>230.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>Value 3</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>0.0</min>
            <max>2400.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 4</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>-20.0</min>
            <max>200.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 5</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>20.0</min>
            <max>700.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>Value 6</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>-3.0</min>
            <max>400.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>Value 7</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>0.0</min>
            <max>10.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>CrS-TMCP:Cryo-PI-31100:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-PI-31100:Val</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-PI-33000:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-PI-33000:Val</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>192</green>
                <blue>203</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-PI-31615:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-PI-31615:Val</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-PI-31200:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-PI-31200:Val</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-TI-33300:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-TI-33300:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-TI-33000:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-TI-33000:Val</name>
            <axis>3</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-FI-31100:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-FI-31100:Val</name>
            <axis>4</axis>
            <color>
                <red>127</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-SI-31131:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-SI-31131:Val</name>
            <axis>2</axis>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-T-31130:CalcdMassflw</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-T-31130:CalcdMassflw</name>
            <axis>4</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-TI-31155:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-TI-31155:Val</name>
            <axis>3</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-PI-31155:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-PI-31155:Val</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>127</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-TI-31130:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-TI-31130:Val</name>
            <axis>3</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>127</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-PI-31130:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-PI-31130:Val</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-GI-31130:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-GI-31130:Val</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-GI-31131:Val</display_name>
            <visible>false</visible>
            <name>CrS-TMCP:Cryo-GI-31131:Val</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>192</green>
                <blue>203</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-TI-31200:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-TI-31200:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>192</green>
                <blue>203</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-TI-31500:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-TI-31500:Val</name>
            <axis>3</axis>
            <color>
                <red>255</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>2.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TMCP:Cryo-TI-33100:Val</display_name>
            <visible>true</visible>
            <name>CrS-TMCP:Cryo-TI-33100:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
    </pvlist>
</databrowser>